import java.awt.Color;

import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.StdOut;

public class SeamCarver {

    private static final double BORDER_ENERGY = 1000;

    private int[][] rgb;
    private double[][] energy;
    private boolean isTransposed;
    private int pictureWidth;
    private int pictureHeight;
    private int width;
    private int height;

    // create a seam carver object based on the given picture
    public SeamCarver(Picture picture) {
        if (picture == null) {
            throw new IllegalArgumentException();
        }
        this.pictureWidth = picture.width();
        this.pictureHeight = picture.height();
        this.width = pictureWidth;
        this.height = pictureHeight;
        if (width > height) {
            this.rgb = new int[width][width];
            this.energy = new double[width][width];
        } else {
            this.rgb = new int[height][height];
            this.energy = new double[height][height];
        }
        this.isTransposed = false;
        for (int row = 0; row <this.height; row++) {
            this.energy[row][0] = BORDER_ENERGY;
            this.energy[row][this.width - 1] = BORDER_ENERGY;
            for (int col = 0; col < this.width; col++) {
                this.rgb[row][col] = picture.getRGB(col, row);
            }
        }
        for (int col = 1; col < this.width - 1; col++) {
            this.energy[0][col] = BORDER_ENERGY;
            this.energy[this.height - 1][col] = BORDER_ENERGY;
        }
    }

    // current picture
    public Picture picture() {
        if (this.isTransposed) {
            this.transpose();
        }
        Picture picture = new Picture(this.width, this.height);
        for (int row = 0; row <this.height; row++) {
            for (int col=0; col < this.width; col++) {
                picture.setRGB(col, row, this.rgb[row][col]);
            }
        }
        return picture;
    }

    private void transpose() {
        this.isTransposed = ! this.isTransposed;
        
        // first swap upper triangle of square
        int minDim = (this.height < this.width) ? this.height : this.width;
        for (int row = 0; row <this.height; row++) {
            for (int col = row + 1; col < this.width; col++) {
                int colour = this.rgb[row][col];
                this.rgb[row][col] = this.rgb[col][row];
                this.rgb[col][row] = colour;
                double nrg = this.energy[row][col];
                this.energy[row][col] = this.energy[col][row];
                this.energy[col][row] = nrg;
            }
        }
        // now swap area outside of square
        if (this.height > minDim) {
            for (int row = minDim; row <this.height; row++) {
                for (int col = 0; col < this.width; col++) {
                    this.rgb[col][row] = this.rgb[row][col];
                    this.energy[col][row] = this.energy[row][col];
                }
            }
        }
        
        if (this.width > minDim) {
            for (int row = 0; row <this.height; row++) {
                for (int col = minDim; col < this.width; col++) {
                    this.rgb[row][col] = this.rgb[col][row];
                    this.energy[row][col] = this.energy[col][row];
                }
            }         
        }
        
        int temp = this.height;
        this.height = this.width;
        this.width = temp;
    }

    // width of current picture
    public int width() {
        return this.pictureWidth;
    }

    // height of current picture
    public int height() {
        return this.pictureHeight;
    }

    // energy of pixel
    public double energy(int col, int row) {
        if (this.isTransposed) {
            this.transpose();
        }
        
        if ((col < 0) || (col >= this.width) || (row < 0) || (row >= this.height)) {
            throw new IllegalArgumentException();
        }
        return calcEnergy(row, col);
    }
    
    private double calcEnergy(int row, int col) {
        double calc = energy[row][col];
        if (calc != 0) {
            return calc;
        }
        if ((col == 0) || (col == this.width - 1) || (row == 0) || (row == this.height - 1)) {
            return BORDER_ENERGY;
        }
        int[] left = rgb(row, col - 1 );
        int[] right = rgb(row, col + 1);
        int[] up = rgb(row - 1, col);
        int[] down = rgb(row + 1, col);
        calc = diffSquared(left[0], right[0]) + diffSquared(up[0], down[0])
                + diffSquared(left[1], right[1]) + diffSquared(up[1], down[1])
                + diffSquared(left[2], right[2]) + diffSquared(up[2], down[2]);
        calc = Math.sqrt(calc);
        energy[row][col] = calc;
        return calc;
    }

    private double diffSquared(int i, int j) {
        double x = (i -j);
        return x * x;
    }

    // get rgb values of a pixel
    private int[] rgb(int row, int col) {
        Color colour = new Color(rgb[row][col]);
        int red = colour.getRed();
        int green = colour.getGreen();
        int blue = colour.getBlue();
        int[] ret = {red, green, blue};
        return ret;
    }

    // sequence of indices for horizontal seam
    public int[] findHorizontalSeam() {
        return findSeam(false);
    }

    // sequence of indices for vertical seam
    public int[] findVerticalSeam() {
         return findSeam(true);
    }

    
    // sequence of indices for seam
    private int[] findSeam(boolean vertical) {
        setOrientation(vertical);

        int[][] parentCol = new int[this.height][this.width];
        double[][] cumEnergy = new double[this.height][this.width];
        for (int col = 0; col < this.width; col++) {
            parentCol[0][col] = -1;
            cumEnergy[0][col] = calcEnergy(0, col);
        }
        
        for (int row = 0; row < this.height - 1; row++) {
            double[] currCum = cumEnergy[row]; 
            double[] nextCum = cumEnergy[row + 1]; 
            int[] nextParent = parentCol[row + 1];
            for (int col = 0; col < this.width; col++) {
                if (currCum[col] == 0) {
                    continue;
                }
                double level = currCum[col];
                
                double newLevel;
                double currentLevel;
                if (col > 0) {
                    newLevel = level + calcEnergy(row + 1, col - 1);
                    currentLevel = nextCum[col - 1];
                    if (currentLevel == 0 || newLevel < currentLevel) {
                        nextCum[col - 1] = newLevel;
                        nextParent[col - 1] = col;
                    }
                }
                
                newLevel = level + calcEnergy(row + 1, col);
                currentLevel = nextCum[col];
                if (currentLevel == 0 || newLevel < currentLevel) {
                    nextCum[col] = newLevel;
                    nextParent[col] = col;
                }
               
                if (col < this.width - 1) {
                    newLevel = level + calcEnergy(row + 1, col + 1);
                    currentLevel = nextCum[col + 1];
                    if (currentLevel == 0 || newLevel < currentLevel) {
                        nextCum[col + 1] = newLevel;
                        nextParent[col + 1] = col;
                    }
                }
               
            }
        }
        
        // now we need to find the minimum cumulative value
        int lastCol = -1;
        int lastRow = this.height - 1;
        double minValue = Double.POSITIVE_INFINITY;
        for (int col = 0; col < this.width; col++) {
            if ( (cumEnergy[lastRow][col] < minValue) && (cumEnergy[lastRow][col] > 0)) {
                minValue = cumEnergy[lastRow][col];
                lastCol = col;
            }
        }
        
        // Now we backtrack to find the seam
        int[] seam = new int[this.height];
        int col = lastCol;
        for (int row = this.height - 1; row > -1; row--) {
            seam[row] = col;
            col = parentCol[row][col];
        }
        return seam;
    }

    private void setOrientation(boolean vertical) {
        if (vertical) {
            if (this.isTransposed) {
                this.transpose();
            }
        } else {
            if (!this.isTransposed) {
                this.transpose();
            }
        }
    }

    // remove horizontal seam from current picture
    public void removeHorizontalSeam(int[] seam) {
        removeSeam(seam, false);
        pictureHeight--;
    }

    
    // remove vertical seam from current picture
    public void removeVerticalSeam(int[] seam) {
        removeSeam(seam, true);
        pictureWidth--;
    }

    
    private void removeSeam(int[] seam, boolean vertical) {
        
        if (seam == null) {
          throw new IllegalArgumentException("null seam");
        }
        
        setOrientation(vertical);

        validateSeam(seam);

      // remove column number in seam
       for (int row = 0; row < this.height; row++) {
          int ignoreCol = seam[row];
          int nextCol = ignoreCol + 1;
          double[] eRow = this.energy[row];
          int rowsToMove = this.width - ignoreCol - 1;
          if (ignoreCol > 0) {
              eRow[ignoreCol - 1] = 0;
          }
          if (nextCol < this.height) {
              eRow[nextCol] = 0;
          }
          System.arraycopy(rgb[row], nextCol, rgb[row], ignoreCol, rowsToMove);
          System.arraycopy(eRow, nextCol, eRow, ignoreCol, rowsToMove);
      }
      this.width--;
    }

    
    private void validateSeam(int[] seam) {
        if (this.width <= 1) {
            throw new IllegalArgumentException("picture width too small");
        }
        if (seam.length != this.height) {
            throw new IllegalArgumentException("wrong seam size");
        }

      int prev = seam[0];
      for (int entry : seam) {
          if ((entry < 0) || (entry >= this.width)
                  || (entry < prev - 1) || (entry > prev + 1)) {
              throw new IllegalArgumentException("wrong seam value " +prev + " " + entry);
          }
          prev = entry;
      }

    }


    //  unit testing (optional)
    public static void main(String[] args) {
        String fileName = "6x5.png";
        Picture pic = new Picture(fileName);
        SeamCarver sc = new SeamCarver(pic);
        int[] vseam = { 3, 4, 3, 2, 1 };
        printHex(pic);
        printHex(sc.picture());
        printSeam(vseam);
        sc.removeVerticalSeam(vseam);
        printHex(sc.picture());

        sc = new SeamCarver(pic);
        StdOut.print("Horizontal seam: ");
        printHex(sc.picture());
        for (int i = 0; i < 3; i++) {
            int[] horizontalSeam = sc.findHorizontalSeam();
            printSeam(horizontalSeam);
            sc.removeHorizontalSeam(horizontalSeam);
            printHex(sc.picture());
        }
      
   }

    private static void printSeam(int[] seam) {
        for (int i : seam) {
            StdOut.print(i);
            StdOut.print("  ");
        }
        StdOut.println();
    }

    private static void printHex(Picture p) {
        StdOut.println();
       for (int row = 0; row < p.height(); row++) {
            for (int col = 0; col < p.width(); col++) {
                int c = p.getRGB(col, row);
                StdOut.printf("%x ",c);
            }
            StdOut.println();
       }
       StdOut.println();
    }

}