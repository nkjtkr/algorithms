import java.util.Comparator;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {
    private boolean solvable;
    private int moves;
    private Stack<Board> solution;
    private Board goal;
    
    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        Comparator<Solver.SearchNode> manhattanComparator = new ManhattanComparator();
        createGoalBoard(initial.dimension());
        MinPQ<SearchNode>  priorityQueue = new MinPQ<>(manhattanComparator);
        SearchNode rootNode = new SearchNode(initial, 0, null);
        priorityQueue.insert(rootNode);

        MinPQ<SearchNode> dualQueue = new MinPQ<>(manhattanComparator);
        SearchNode dualRoot = new SearchNode(initial.twin(), 0, null);
        dualQueue.insert(dualRoot);

        solvable = false;
        moves = -1;
        SearchNode lastNode = null;
        while (!priorityQueue.isEmpty()) {
            // first check primary queue
            SearchNode node = priorityQueue.delMin();
            if (node.board.equals(goal)) {
                lastNode = node;
                solvable = true;
                break;
            }
            for (Board board : node.board.neighbors()) {
                SearchNode child = new SearchNode(board, node.moves + 1, node);
                if (!child.hasLoop()) {
                    priorityQueue.insert(child);
                }
            }

            // now check dual queue
            if (dualQueue.isEmpty()) {
                continue;  // keep on looking for primal solution 
            }
            node = dualQueue.delMin();
            if (node.board.isGoal()) {
                return;  // no primal solution
            }
            for (Board board : node.board.neighbors()) {
                SearchNode child = new SearchNode(board, node.moves + 1, node);
                if (!child.hasLoop()) {
                    dualQueue.insert(child);    
                }
            }
        }
        if (lastNode != null) {
            this.moves = lastNode.moves;
            solution = getSolution(lastNode);
        }
    }
    
    private void createGoalBoard(int dimension) {
        int[][] t = new int[dimension][dimension];
        int k = 1;
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                t[i][j] = k++;
            }
        }
        t[dimension - 1][dimension - 1] = 0;
        goal = new Board(t);
    }

    private Stack<Board> getSolution(SearchNode lastNode) {
        Stack<Board> soln = new Stack<>();
        SearchNode node = lastNode;
           while (node != null) {
               soln.push(node.board);
               node = node.prev;
           }
        return soln;
    }

    // is the initial board solvable?
    public boolean isSolvable() {
         return solvable;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves() {
        return moves;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution() {
        if (!solvable) {
            return null;
        }
        return this.solution;
     }

    private class SearchNode {
        private final Board board;
        private final int moves;
        private final SearchNode prev;
        private final int manhattan;
        
        public SearchNode(Board board, int moves, SearchNode prev) {
            this.board = board;
            this.moves = moves;
            this.prev = prev;
            this.manhattan = board.manhattan() + moves;
        }

        // is this board already in our search tree ?
        public boolean hasLoop() {
            Solver.SearchNode other = this.prev;
            while (other != null) {
                if (this.board.equals(other.board)) {
                    return true;
                }
                other = other.prev;
            }
            return false;
        }
        
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(board.toString());
            sb.append(" moves:");
            sb.append(moves);
            sb.append(" priority:");
            sb.append(manhattan);
            SearchNode parent = prev;
            while (parent != null) {
                sb.append(" prev->");
                sb.append(parent.board.toString());
                parent = parent.prev;
            }
            return sb.toString();
        }
    }
    
    // Note: this comparator imposes orderings that are inconsistent with equals.
    private class ManhattanComparator implements Comparator<SearchNode> {
        @Override
        public int compare(SearchNode arg0, SearchNode arg1) {
            return (arg0.manhattan - arg1.manhattan);
        }
    }

    // test client (see below) 
     public static void main(String[] args) {

        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                tiles[i][j] = in.readInt();
            }
        }
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable()) {
            StdOut.println("No solution possible");
        } else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution()) {
                StdOut.println(board);
            }
        }
    }
}
