import java.util.Arrays;

import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class Board {

    private static final int EMPTY = -1;
    private final int n;
    private final int numCells;
    private final int emptyIndex;
    private final int[] tiles;
    private Board twin = null;

    // create a board from an n-by-n array of tiles,
    // where tiles[row][col] = tile at (row, col)

    // flatten array 
    // subtract 1 from one-based values to match zero-based indecies
    public Board(int[][] tiles) {
        if (tiles == null) {
            throw new java.lang.IllegalArgumentException();
        }
        int emptyCell = 0;
        n = tiles.length;
        numCells = n * n;
        this.tiles = new int[numCells];
        int k = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int value = tiles[i][j] - 1;
                if (value == EMPTY) {
                    emptyCell = k;
                }
                this.tiles[k++] = value;
            }
        }
        this.emptyIndex = emptyCell;
    }
    
    // A new copy of this board with 2 cells swapped over 
    private Board(Board that, int index1, int index2) {
        this.n = that.n;
        this.numCells = that.numCells;
        this.tiles =  Arrays.copyOf(that.tiles, that.tiles.length);
        this.tiles[index1] = that.tiles[index2];
        this.tiles[index2] = that.tiles[index1];
        if (this.tiles[index1] == EMPTY) {
            this.emptyIndex = index1;           
        } else if (this.tiles[index2] == EMPTY) {
                this.emptyIndex = index2;           
        } else {
            this.emptyIndex = that.emptyIndex;
        }

    }
                                          
    // string representation of this board
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.n);
        sb.append('\n');
        int cols = 0;
        for (int i = 0; i < numCells; i++) {
            sb.append(' ');
            int value =  tiles[i] + 1;
            if (value < 10) {
                sb.append("   ");
            } else if (value < 100) {
                sb.append("  ");
            } else {
                sb.append(' ');
            }
            sb.append(value);
            if (++cols == n) {
                sb.append('\n');
                cols = 0;
            }
        }
         return sb.toString();
    }

    // board dimension n
    public int dimension() {
        return n;
    }

    // number of tiles out of place
    public int hamming() {
        int distance = 0;
        for (int i = 0; i < numCells; i++) {
            int value = tiles[i];
            if ((value != EMPTY) && (value != i)) {
                distance++;
            }
        }
        return distance;
    }

    // sum of Manhattan distances between tiles and goal
    public int manhattan() {
        int distance = 0;
        for (int i = 0; i < numCells; i++) {
            int value = tiles[i];
            if ((value == EMPTY) || (value == i)) {
                continue;
            }
            int rowError = row(i) - row(value);
            int colError = col(i) - col(value);
            if (rowError > 0) {
                distance += rowError;
            } else {
                distance -= rowError;
            }
            if (colError > 0) {
                distance += colError;
            } else {
                distance -= colError;
            }
            
        }
        return distance;
    }

    // is this board the goal board?
    public boolean isGoal() {
        for (int i = 0; i < numCells; i++) {
            int actual = tiles[i];
            if ((actual != EMPTY) && (actual != i)) {
                return false;
            }
        }
        return true;
    }

    // does this board equal ?
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Board that = (Board) obj;
        if (this.dimension() != that.dimension()) {
            return false;
        }
        return Arrays.equals(this.tiles, that.tiles);
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        Queue<Board> neighbors = new Queue<>();
        int zeroRow = row(emptyIndex);
        int zeroCol = col(emptyIndex);
        // up
        if (zeroRow > 0) {
            Board b = new Board(this, emptyIndex, emptyIndex - n);
            neighbors.enqueue(b);
        }
        // down
        if (zeroRow < n - 1) {
            Board b = new Board(this, emptyIndex, emptyIndex + n);
            neighbors.enqueue(b);
        }
        // left
        if (zeroCol > 0) {
            Board b = new Board(this, emptyIndex, emptyIndex - 1);
            neighbors.enqueue(b);
        }
        // right
        if (zeroCol < n - 1) {
            Board b = new Board(this, emptyIndex, emptyIndex + 1);
            neighbors.enqueue(b);
        }
        return neighbors;
    }

    // a board that is obtained by exchanging any pair of tiles
    public Board twin() {
        // StdRandom.uniform(n) returns a random number between 0 (inclusive) and n (exclusive)
        if (twin != null) {
            return twin;
        }

        int from;
        while (true) {
            from = StdRandom.uniform(numCells);
            if (tiles[from] != EMPTY) {
                break;
            }
        }
        int to;
        while (true) {
            to = StdRandom.uniform(numCells);
            if ((to != from) && (tiles[to] != EMPTY)) {
                break;
            }
        }
        twin = new Board(this, from, to);
        return twin;
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        int[][] arr0 = {{1, 2, 3}, {4, 0, 5}, {6, 7, 8} };
        Board board0 = new Board(arr0);
        StdOut.println(board0.toString());
        StdOut.println("Neighbours:");
        for (Board b : board0.neighbors()) {
            StdOut.println(b.toString());
        }
        StdOut.println("empty:" + board0.emptyIndex);
        int[][] arr = {{8, 2, 0}, {1, 5, 3}, {7, 4, 6} };
        Board board = new Board(arr);
        StdOut.println(board.toString());
        StdOut.println("Dimension:" + board.dimension());
        StdOut.println("empty:" + board.emptyIndex);
        StdOut.println("Hamming distance:" + board.hamming());
        StdOut.println("Manhattan distance:" + board.manhattan());
        StdOut.println("Soved:" + board.isGoal());
        StdOut.println("Neighbours:");
        for (Board b : board.neighbors()) {
            StdOut.println(b.toString());
        }
        StdOut.println("Twin:");
        StdOut.println(board.twin());
        StdOut.println("Twin:");
        StdOut.println(board.twin());
       
        Board board2 = new Board(arr);
        StdOut.println("Board " + board.hashCode() 
                + " equals Board " + board2.hashCode() 
                + " " + board.equals(board2) + " ?");

        int[][] arr2 = {{8, 2, 0}, {1, 3, 5}, {7, 4, 6} };
        board2 = new Board(arr2);
        StdOut.println("Board " + board.hashCode()
                + " not equals Board " + board2.hashCode() 
                + " " + board.equals(board2) + " ?");

   }

    private int row(int index) {
        return (index / n);
    }  
    
    private int col(int index) {
        return (index % n);
    }

}
