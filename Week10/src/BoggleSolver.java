import java.util.ArrayList;
import java.util.Iterator;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;

public class BoggleSolver  {
    private static final int R = 26;
    private static final int A_VALUE = 65;
    private final Trie dictionary;

    // Initializes the data structure using the given array of strings as the dictionary.
    // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
    public BoggleSolver(String[] dictionary) {
        this.dictionary = new Trie();
        for (String word : dictionary) {
            this.dictionary.add(word);
        }
    }

    // Returns the set of all valid words in the given Boggle board, as an Iterable.
    public Iterable<String> getAllValidWords(BoggleBoard board) {
        int numChars = board.rows() * board.cols();
        char[] letters = new char[numChars];  // flattened board
        int[][] adj = new int[numChars][];    // flattened adjacency list

        // flatten board and create adjaceny lists;
        int k = 0;
        int numRows = board.rows();
        int maxRow = numChars - 1;
        int numCols = board.cols();
        int maxCol = numCols - 1;
        int curRow = 0;
        ArrayList<Integer> arr = new ArrayList<>();
        for (int row = 0; row < numRows; row++) {
            int prevRow = curRow - numCols;
            int nextRow = curRow + numCols;
            for (int col = 0; col < board.cols(); col++) {
                letters[k] = board.getLetter(row, col);
                int prevCol = col - 1;
                int nextCol = col + 1;
                arr.clear();

                if (prevRow >= 0) {
                    if (prevCol >= 0) {
                        arr.add(prevRow + prevCol);
                    }
                    arr.add(prevRow + col);
                    if (nextCol <= maxCol) {
                        arr.add(prevRow + nextCol);
                    }
                }

                if (prevCol >= 0) {
                    arr.add(curRow + prevCol);
                }
                if (nextCol <= maxCol) {
                    arr.add(curRow + nextCol);
                }

                if (nextRow <= maxRow) {
                    if (prevCol >= 0) {
                        arr.add(nextRow + prevCol);
                    }
                    arr.add(nextRow + col);
                    if (nextCol <= maxCol) {
                        arr.add(nextRow + nextCol);
                    }
                }

                int[] adjrow = new int[arr.size()];
                for (int x = 0; x < arr.size(); x++) {
                    adjrow[x] = arr.get(x);    
                }
                adj[k++] = adjrow;
            }
            curRow += numCols;
        }

        Trie validWords = new Trie();
        StringBuilder sb = new StringBuilder(); // use to build up a word
        boolean[] visited = new boolean[numChars];
        for (k = 0; k < numChars; k++) {
            check(letters, k, sb, visited, validWords, adj);

        }
        return validWords;
    }

    private void check(char[] letters, int k, StringBuilder sb, 
            boolean[] visited, Trie validWords, int[][] adj) {
        char c = letters[k];
        sb.append(c);
        if (c == 'Q') {
            sb.append('U');
        }
        visited[k] = true;

        String str = sb.toString();
        if (sb.length() > 2) {
            if (this.dictionary.contains(str)) {
                validWords.add(str);
            }
        }

        if (this.dictionary.isPrefix(str)) {
            for (int i : adj[k]) {
                if (!visited[i]) {
                    check(letters, i, sb, visited, validWords, adj);
                }
            }
        }

        if (c == 'Q') {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.deleteCharAt(sb.length() - 1);
        visited[k] = false;
    }

    // Returns the score of the given word if it is in the dictionary, zero otherwise.
    // (You can assume the word contains only the uppercase letters A through Z.)
    public int scoreOf(String word) {
        if (dictionary.contains(word)) {
            switch (word.length()) {
            case 3:
            case 4:
                return 1;
            case 5:
                return 2;
            case 6:
                return 3;
            case 7:
                return 5;
            case 1:
            case 2:
                return 0;
            default:
                return 11;
            }
        }
        return 0;
    }


    /*
     * arg 0 - dictionary file
     * arg 1 - board
     */
    public static void main(String[] args) {
        In in = new In(args[0]);
        String[] dictionary = in.readAllStrings();
        BoggleSolver solver = new BoggleSolver(dictionary);

        BoggleBoard board = new BoggleBoard(args[1]);
        int score = 0;
        for (String word : solver.getAllValidWords(board)) {
            StdOut.println(word);
            score += solver.scoreOf(word);
        }
        StdOut.println("Score = " + score);
    }

    private static class Node {
        private Node[] next = new Node[R];
        private boolean isString;
    }

    private static class Trie  implements Iterable<String> {
        Node root; 

        /**
         * Adds the key to the set if it is not already present.
         * @param key the key to add
         * @throws IllegalArgumentException if {@code key} is {@code null}
         */
        public void add(String key) {
            if (key == null) throw new IllegalArgumentException("argument to add() is null");
            root = add(root, key, 0);
        }

        private Node add(Node x, String key, int d) {
            if (x == null) x = new Node();
            if (d == key.length()) {
                x.isString = true;
            } else {
                int c = key.charAt(d) - A_VALUE;
                x.next[c] = add(x.next[c], key, d + 1);
            }
            return x;
        }

        /**
         * Does the set contain the given key?
         * @param key the key
         * @return {@code true} if the set contains {@code key} and
         *     {@code false} otherwise
         * @throws IllegalArgumentException if {@code key} is {@code null}
         */
        public boolean contains(String key) {
            if (key == null) throw new IllegalArgumentException("argument to contains() is null");
            Node x = get(root, key, 0);
            if (x == null) return false;
            return x.isString;
        }

        private Node get(Node x, String key, int d) {
            if (x == null) return null;
            if (d == key.length()) return x;
            int c = key.charAt(d) - A_VALUE;
            return get(x.next[c], key, d + 1);
        }

        /**
         * Returns all of the keys in the set, as an iterator.
         * To iterate over all of the keys in a set named {@code set}, use the
         * foreach notation: {@code for (Key key : set)}.
         * @return an iterator to all of the keys in the set
         */
        public Iterator<String> iterator() {
            return keysWithPrefix("").iterator();
        }

        /**
         * Returns all of the keys in the set that start with {@code prefix}.
         * @param prefix the prefix
         * @return all of the keys in the set that start with {@code prefix},
         *     as an iterable
         */
        public Iterable<String> keysWithPrefix(String prefix) {
            Queue<String> results = new Queue<String>();
            Node x = get(root, prefix, 0);
            collect(x, new StringBuilder(prefix), results);
            return results;
        }

        private void collect(Node x, StringBuilder prefix, Queue<String> results) {
            if (x == null) return;
            if (x.isString) results.enqueue(prefix.toString());
            for (char c = 0; c < R; c++) {
                prefix.append((char) (c + A_VALUE));
                collect(x.next[c], prefix, results);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        }

        /**
         * Does the set contain the given string as a prefix?
         * @param prefix the prefix
         * @return {@code true} if the set contains {@code prefix} 
         * as the prefix to a word and
         *     {@code false} otherwise
         * @throws IllegalArgumentException if {@code prefix} is {@code null}
         */
        public boolean isPrefix(String prefix) {
            if (prefix == null) throw new IllegalArgumentException("argument to contains() is null");
            Node x = get(root, prefix, 0);
            if (x == null) return false;
            for (Node y : x.next) {
                if (y != null) return true;
            }
            return false;
        }

    }

}
