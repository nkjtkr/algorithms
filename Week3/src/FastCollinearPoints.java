import java.util.Arrays;
import java.util.Comparator;

public class FastCollinearPoints {
    private SegmentList segments = new SegmentList();

    public FastCollinearPoints(Point[] points) {   // finds all line segments containing 4 points
        for (int i = 0; i < points.length; i++) {
        	Point p = points[i];
        	Point[] others = excludePoint(points, i);
        	Comparator<Point> cmp = p.slopeOrder();
        	Arrays.sort(others, cmp);
        	if (p.compareTo(others[0]) == 0) {
            	// duplicated points will sort have a slope of NEGATIVE_INFINITY and sort to front
        		throw new IllegalArgumentException("repeated point found");
        	}
        	findCollinear(p, others);
        }
    	
    }
    
	private void findCollinear(Point p, Point[] others) {
    	// points are collinear if 3 or more have the same slope
    	// to prevent duplication current point must be lowest on line segment
    	double lineSlope = Double.NEGATIVE_INFINITY;
    	MySegment seg = new MySegment(p);
    	for (Point q : others) {
    		double currentSlope = p.slopeTo(q);
     		if (currentSlope == lineSlope) {
     			seg.addPoint(q);
    			continue;
    		}
			if (seg.isValid()) {
				LineSegment line = seg.toLineSegment();
    			segments.add(line);
			}
			seg = new MySegment(p);
			seg.addPoint(q);
			lineSlope = currentSlope;
    	}
		if (seg.isValid()) {
			LineSegment line = seg.toLineSegment();
			segments.add(line);
		}
	}
	
	private class MySegment {
		Point referencePoint;
		Point startPoint;
		Point endPoint;
		int count;
		
		MySegment(Point p) {
			referencePoint = p;
			startPoint = p;
			endPoint = p;
			count = 1;
		}
		
		void addPoint(Point p) {
			count++;
			if (p.compareTo(startPoint) < 0) {
				startPoint = p;
			}
			if (p.compareTo(endPoint) > 0) {
				endPoint = p;
			}
		}

		boolean isValid() {
			return((count >= 4) && (startPoint.compareTo(referencePoint) == 0));
		}
		
		LineSegment toLineSegment() {
			return new LineSegment(startPoint, endPoint);
		}
	}

	private Point[] excludePoint(Point[] points, int index) {
		Point[] others = new Point[points.length - 1];
    	int k = 0;
    	for (int j = 0; j < points.length; j++) {
    		if (index != j) {
    			others[k++] =  points[j];
    		}
    	}
		return others;
	}

	public int numberOfSegments() {       // the number of line segments
        return segments.numSegments;
    }
    
    public LineSegment[] segments() {               // the line segments
        return segments.toArray();
    }

    private class SegmentList {
        int numSegments = 0;
        private ListEntry firstSegment = null;
        private ListEntry lastSegment = null;
        
        void add(LineSegment seg) {
            ListEntry newEntry = new ListEntry(seg);
            if (numSegments == 0) {
                firstSegment = newEntry;
            } else {
                lastSegment.next = newEntry;
            }
            lastSegment = newEntry;
            numSegments++;
        }
        
        LineSegment[] toArray() {
            LineSegment[] array = new LineSegment[numSegments];
            ListEntry entry = firstSegment;
            int i = 0;
            while (entry != null) {
                array[i++] = entry.seg;
                entry = entry.next;
            }
            return array;
        }
        private class ListEntry {
            ListEntry next;
            LineSegment seg;
            
            ListEntry(LineSegment seg) {
                this.next = null;
                this.seg = seg;
            }        
        }
    }
}
