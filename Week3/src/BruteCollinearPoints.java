
public class BruteCollinearPoints {
    private SegmentList segments = new SegmentList();

    public BruteCollinearPoints(Point[] points) {   // finds all line segments containing 4 points
        checkForDuplicates(points);
        if (points.length < 4) {
            return;
        }
        for (int i = 0; i < points.length - 3; i++) {
            for (int j = i+1; j < points.length - 2; j++) {
                for (int k = j + 1; k < points.length - 1; k++) {
                    for (int m = k + 1; m < points.length; m++) {
                        LineSegment seg = collinear(points[i], points[j], points[k], points[m]);
                        if (seg != null) {
                            segments.add(seg);
                        }
                    }
                }
            }
        }
    }
    
    private LineSegment collinear(Point p, Point q, Point r, Point s) {
        Point[] pts = {p, q, r, s};
        Point min = p;
        Point max = p;
        for (int i = 1; i<4; i++) {
            if (min.compareTo(pts[i]) > 0) {
                min = pts[i];
            }
            if (max.compareTo(pts[i]) < 0) {
                max = pts[i];
            }
        }        
        double slope = min.slopeTo(max);
        for (int i = 0; i < 4; i++) {
            Point pt = pts[i];
            if ((pt == min) || (pt == max)) {
                continue;
            }
            if (min.slopeTo(pt) != slope) {
                return null;
            }     
        }
        return new LineSegment(min, max);
   }

    private void checkForDuplicates(Point[] sortedPoints) {
        Point prev = null;
        for (Point p : sortedPoints) {
            if ((prev != null) && (prev.compareTo(p) == 0)){
                throw new IllegalArgumentException("points must not be duplicated.");
            }
            prev = p;          
        }
    }
    
    public int numberOfSegments() {       // the number of line segments
        return segments.numSegments;
    }
    
    public LineSegment[] segments() {               // the line segments
        return segments.toArray();
    }

    
    private class SegmentList {
        int numSegments = 0;
        private ListEntry firstSegment = null;
        private ListEntry lastSegment = null;
        
        void add(LineSegment seg) {
            ListEntry newEntry = new ListEntry(seg);
            if (numSegments == 0) {
                firstSegment = newEntry;
            } else {
                lastSegment.next = newEntry;
            }
            lastSegment = newEntry;
            numSegments++;
        }
        
        LineSegment[] toArray() {
            LineSegment[] array = new LineSegment[numSegments];
            ListEntry entry = firstSegment;
            int i = 0;
            while (entry != null) {
                array[i++] = entry.seg;
                entry = entry.next;
            }
            return array;
        }
        private class ListEntry {
            ListEntry next;
            LineSegment seg;
            
            ListEntry(LineSegment seg) {
                this.next = null;
                this.seg = seg;
            }        
        }
    }
}
