import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class BurrowsWheeler {
    // apply Burrows-Wheeler transform,
    // reading from standard input and writing to standard output 
    public static void transform() {
        StringBuilder sb = new StringBuilder();
        while (!BinaryStdIn.isEmpty()) {
            char inp = BinaryStdIn.readChar();
            sb.append(inp);
        }
        CircularSuffixArray csa = new CircularSuffixArray(sb.toString());
        int length = csa.length();
        for (int i = 0; i < length; i++) {
            if (csa.index(i) == 0) {
                BinaryStdOut.write(i);
                break;
            }
        }
        for (int i = 0; i < length; i++) {
            int k = csa.index(i) - 1;
            if (k < 0) {
                k += length;
            }
            BinaryStdOut.write(sb.charAt(k));
        }
        BinaryStdOut.close();
    }

    // apply Burrows-Wheeler inverse transform,
    // reading from standard input and writing to standard output
    public static void inverseTransform() {
        int first = BinaryStdIn.readInt();
        String str = BinaryStdIn.readString();
        char[] inp = str.toCharArray();
        BinaryStdIn.close();
        int length = inp.length;
        int[] next = new int[length];
        int[] count = new int[257];
        
        for (int i = 0; i < length; i++) {
            count[inp[i] + 1]++;
        }
        
        for (int i = 0; i < 256; i++) {
            count[i+1] += count[i];
        }
        
        for (int i = 0; i < length; i++) {
            next[count[inp[i]]++] = i;
        }
        int i = next[first];
        while (i != first) {
            BinaryStdOut.write(inp[i]);
            i = next[i];
        }
        BinaryStdOut.write(inp[first]);
        BinaryStdOut.close();
    }

    // if args[0] is "-", apply Burrows-Wheeler transform
    // if args[0] is "+", apply Burrows-Wheeler inverse transform
    public static void main(String[] args) {
        if (args.length > 0) {
            if ("+".equals(args[0])) {
                inverseTransform();
            } else if ("-".equals(args[0])) {
                transform();
            }
        }
    }

}
