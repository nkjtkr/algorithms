import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

public class MoveToFront {
    // apply move-to-front encoding, reading from standard input and writing to standard output
    public static void encode() {
        char[] code = new char[256];        // front to back character array
        int[] index = new int[256];            // index of character
        for (int i = 0; i < 256; i++) {
            code[i] = (char) i;
            index[i] = i;
        }
        while (!BinaryStdIn.isEmpty()) {
            char inp = BinaryStdIn.readChar();
            char out = (char) index[inp];  // get position of input character in front to back array
            for (int i = out - 1; i >= 0; i--) {   // shuffle right by one position to make space at pos 0
                int next = i + 1;
                char ch = code[i];
                code[next] = ch;
                index[ch] = next;
            }
            code[0] = inp; // move input character to front
            index[inp] = 0;
            BinaryStdOut.write(out);
         }
        BinaryStdIn.close();
        BinaryStdOut.close();
    }

    // apply move-to-front decoding, reading from standard input and writing to standard output
    public static void decode() {
        char[] code = new char[256];        // front to back character array
        for (int i = 0; i < 256; i++) {
            code[i] = (char) i;
        }
        while (!BinaryStdIn.isEmpty()) {
            int inp = BinaryStdIn.readChar(); // this is the position of the original character
            char out = code[inp];
            for (int i = inp - 1; i >= 0; i--) {   // shuffle right by one position to make space at pos 0
                int next = i + 1;
                char ch = code[i];
                code[next] = ch;
            }
            code[0] = out; // move output character to front
            BinaryStdOut.write(out);
        }
        BinaryStdIn.close();
        BinaryStdOut.close();
     }

    // if args[0] is "-", apply move-to-front encoding
    // if args[0] is "+", apply move-to-front decoding
    // java-algs4 MoveToFront - < abra.txt | java-algs4 MoveToFront +
    public static void main(String[] args) {
        if (args.length > 0) {
            if ("+".equals(args[0])) {
                decode();
            } else if ("-".equals(args[0])) {
                encode();
            }
        }
    }

}
