import java.util.Arrays;

import edu.princeton.cs.algs4.StdOut;

public class CircularSuffixArray {
    private final int length;
    private final char[] buffer;
    private final CircularSuffix[] suffixes;

    // circular suffix array of s
    public CircularSuffixArray(String s) {
        if (s == null) {
            throw new IllegalArgumentException("null string");
        }
        this.length = s.length();       
           this.buffer = s.toCharArray();
           this.suffixes = new CircularSuffix[this.length];
           for (int i = 0; i < this.length; i++) {
               this.suffixes[i] = new CircularSuffix(i);
           }
           Arrays.sort(this.suffixes);
    }

    // length of s
    public int length() {
        return length;
    }

    // returns index of ith sorted suffix
    public int index(int i) {
        if ((i < 0) || (i >= length)) {
            throw new IllegalArgumentException("invalid index value " + i);
        }
        return this.suffixes[i].start;
    }

    // unit testing (required)
    public static void main(String[] args) {
        CircularSuffixArray cs = new CircularSuffixArray("ABRACADABRA!");
        for (int i = 0; i < cs.length(); i++) {
            StdOut.print(i);
            StdOut.print(":");
            StdOut.println(cs.index(i));
        }
    }

    private class CircularSuffix implements Comparable<CircularSuffix> {
        private final int start;

        public CircularSuffix(int start) {
            this.start = start;
         }

        @Override
        public int compareTo(CircularSuffix other) {
            if (this.start == other.start) {
                return 0;
            }
            int j = this.start;
            int k = other.start;
            for (int i = 0; i < length; i++) {
                int value = buffer[j] - buffer[k];
                if (value > 0) {
                    return 1;
                }
                if (value < 0) {
                    return -1;
                }
                
                if (++j >= length) {
                    j = 0;
                }
                if (++k >= length) {
                    k = 0;
                }
            }
            return 0;  // should not reach this line
        }
    }
}
