import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    
    private final int n;
    private final int trials;
    private double mean;
    private double stdDev;
    private double upperConfidence;
    private double lowerConfidence;
    
    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0) {
            throw new IllegalArgumentException("Illegal n paraameter");
        }
        if (trials <= 0) {
            throw new IllegalArgumentException("Illegal trials paraameter");
        }
        this.n = n;
        this.trials = trials;
        process();
    }

    // sample mean of percolation threshold
    public double mean() {
        return this.mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return this.stdDev;
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        return this.lowerConfidence;
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return this.upperConfidence;
    }

    // test client (see below)
    public static void main(String[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("Incorrect number of command line parameters.");
        }
        int n;
        int t;
        n = Integer.parseInt(args[0]);
        t = Integer.parseInt(args[1]);
        PercolationStats proc = new PercolationStats(n, t);
        StdOut.printf("mean                    = %f\n", proc.mean());
        StdOut.printf("stddev                  = %f\n", proc.stddev());
        StdOut.printf("95%% confidence interval = [%f, %f]\n", proc.confidenceLo(), proc.confidenceHi());        
    }

    private void process() {
        double[] results = new double[this.trials];
        for (int i = 0; i < this.trials; i++) {
            results[i] = runTrial();
        }
        calculateStats(results);
    }

    private void calculateStats(double[] results) {
        if (this.trials == 1) {
            this.mean = results[0];
            this.stdDev = Double.NaN;
            this.lowerConfidence = Double.NaN;
            this.upperConfidence = Double.NaN;
            return;
        }
        this.mean = StdStats.mean(results);
        this.stdDev = StdStats.stddev(results);
        double temp = 1.96 * this.stdDev / Math.sqrt(this.trials);
        this.lowerConfidence = this.mean - temp;
        this.upperConfidence = this.mean + temp;
    }

    private double runTrial() {
        Percolation p = new Percolation(n);
        while (!p.percolates()) {
            int row = StdRandom.uniform(1, n + 1);
            int col = StdRandom.uniform(1, n + 1);
            p.open(row, col);
        }
        return ((double) p.numberOfOpenSites()) / ((double) (this.n * this.n));
    }
}
