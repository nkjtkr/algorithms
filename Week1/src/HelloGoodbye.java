
public class HelloGoodbye {

	public static void main(String[] args) {
		if (args.length == 2) {
			System.out.print("Hello ");
			System.out.print(args[0]);
			System.out.print(" and ");
			System.out.print(args[1]);
			System.out.println('.');

			System.out.print("Goodbye ");
			System.out.print(args[1]);
			System.out.print(" and ");
			System.out.print(args[0]);
			System.out.println('.');
		} else {
			System.out.println("Wrong number of arguments");
		}
	}
}
