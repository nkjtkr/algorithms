
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class RandomWord {

	public static void main(String[] args) {
		String selected = "";
		int i=1;
		while(!StdIn.isEmpty()) {
			String word = StdIn.readString();
			double p = 1.0 / i;
			if (StdRandom.bernoulli(p)) {
				selected = word;
			}
			i++;
		}
		System.out.println(selected);
	}

}
