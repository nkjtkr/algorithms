import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private final int numRows;
    private final int numCols;
    private int numOpenSites;
    private final int virtualTopIndex;
    private final int virtualBottomIndex;
    private final int virtualTopIndex2;
    private final int virtualBottomIndex2;
    private final int offset;
    private boolean[] sites;
    private final WeightedQuickUnionUF quickFind;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Constructor parameter must be greater than zero.");
        }
        this.numRows = n;
        this.numCols = n;
        int numCells = 2 * (this.numRows * this.numCols) + 4; // allow for 4 virtual cells
        this.offset = this.numRows * this.numCols + 2;
        this.virtualTopIndex = 0;
        this.virtualBottomIndex = this.virtualTopIndex + (this.numRows * this.numCols) + 1;
        this.virtualTopIndex2 = this.virtualTopIndex + offset;
        this.virtualBottomIndex2 = this.virtualBottomIndex + offset;
        this.sites = new boolean[numCells];
        for (int i = 0; i < numCells; i++) {
            this.sites[i] = false;
        }
        this.sites[virtualTopIndex] = true;
        this.sites[virtualTopIndex2] = true;
        this.sites[virtualBottomIndex] = false;
        this.sites[virtualBottomIndex2] = true;
        this.numOpenSites = 0;
        quickFind = new WeightedQuickUnionUF(numCells);
    }
    
    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        validateRowAndCol(row, col);
        int i =  index(row, col);
        if (isOpen(i)) {
            return;
        }

        this.sites[i] = true;
        this.sites[i + offset] = true;
        this.numOpenSites++;

        int j = cellAbove(row, col);
        if (this.isOpen(j)) {
            connect(i, j);
        }
        
        j = cellBelow(row, col);
        if (this.isOpen(j + offset)) {
            connect(i, j);
        }

        j = cellLeft(row, col);
        if (this.isOpen(j)) {
            connect(i, j);
        }
 
        j = cellRight(row, col);
        if (this.isOpen(j)) {
            connect(i, j);
        }
    }
    
    private void connect(int i, int j) {
        if (j != virtualBottomIndex) {
            quickFind.union(i, j);
        }
        quickFind.union(i + offset, j + offset);
    }
    
    private int cellAbove(int row, int col) {
        if (row == 1) {
            return this.virtualTopIndex;
        }
        return index(row - 1, col);
    }
    
    private int cellBelow(int row, int col) {
        if (row == this.numRows) {
            return virtualBottomIndex;
        }
        return index(row + 1, col);
    }
    
    private int cellLeft(int row, int col) {
        if (col == 1) {
            return -1;
        }
        return index(row, col - 1);
    }
    
    private int cellRight(int row, int col) {
        if (col == this.numCols) {
            return -1;
        }
        return index(row, col + 1);
    }
    
    private void validateRowAndCol(int row, int col) {
        if (row <= 0 || row > this.numRows) {
            throw new IllegalArgumentException("Illegal row value.");
        }
        if (col <= 0 || col > this.numCols) {
            throw new IllegalArgumentException("Illegal col value.");
        }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        validateRowAndCol(row, col);
        int i =  index(row, col);
        return (this.sites[i]);
    }
    
    // is the site (index) open?
    private boolean isOpen(int index) {
        if (index < 0) {
            return false;
        }
        return (this.sites[index]);
    }
    
    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        validateRowAndCol(row, col);
        int i =  index(row, col);
        return (this.quickFind.find(virtualTopIndex) == this.quickFind.find(i));
    }
    
    // returns the number of open sites
    public int numberOfOpenSites() {
        return this.numOpenSites;
    }
    
    // does the system percolate?
    public boolean percolates() {
        return (this.quickFind.find(virtualTopIndex2) == this.quickFind.find(virtualBottomIndex2));
    }
    
    // generate a unique id for a row and column
    // minimum value = 1
    // index 0 reserved
    private int index(int row, int col) {
        return (row - 1) * this.numCols + col;
    }
    
    // test client (optional)
    public static void main(String[] args) {
        int n = 5;
        int tries = 15;
        Percolation p = new Percolation(n);
        for (int i = 0; i < tries; i++) {
            int row = StdRandom.uniform(1, n);
            int col = StdRandom.uniform(1, n);
            p.open(row, col);
        }
    }
}
