import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;
import java.util.NoSuchElementException;

// Memory usage
// class overhead: 16
// pointers: 2x8
// count:   4
// padding: 4
// Total: 40
public class Deque<Item> implements Iterable<Item> {
    private Element firstItem = null;
    private Element lastItem = null;
    private int entryCount = 0;
    
    // construct an empty deque
    public Deque() {}

    // is the deque empty?
    public boolean isEmpty() {
         return (this.firstItem == null);
    }

    // return the number of items on the deque
    public int size() {
        return entryCount;
    }


    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot add a null item.");
        }
        Element oldFirstItem = firstItem;
        Element newElt = new Element();
        newElt.data = item;
        newElt.prevItem = null;
        newElt.nextItem = oldFirstItem;
        if (oldFirstItem != null) {
            oldFirstItem.prevItem = newElt;
        }
        firstItem = newElt;
        if (lastItem == null) {
            lastItem = newElt;
        }
        entryCount++;
    }


    // add the item to the back
    public void addLast(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot add a null item.");
        }
        Element oldLastItem = lastItem;
        Element newElt = new Element();
        newElt.data = item;
        newElt.prevItem = oldLastItem;
        newElt.nextItem = null;
        if (oldLastItem != null) {
            oldLastItem.nextItem = newElt;
        }
        lastItem = newElt;
        if (firstItem == null) {
            firstItem = newElt;
        }
        entryCount++;
    }


    // remove and return the item from the front
    public Item removeFirst() {
        if (entryCount == 0) {
            throw new NoSuchElementException("Nothing to remove");
        }
        Element oldFirst = firstItem;
        firstItem = oldFirst.nextItem;
        if (firstItem == null) {
            lastItem = null;
        } else {
            firstItem.prevItem = null;
        }
        entryCount--;
        return oldFirst.data;
    }


    // remove and return the item from the back
    public Item removeLast() {
        if (entryCount == 0) {
            throw new NoSuchElementException("Nothing to remove");
        }
        Element oldLast = lastItem;
        lastItem = oldLast.prevItem;
        if (lastItem == null) {
            firstItem = null;
        } else {
            lastItem.nextItem = null;
        }
        entryCount--;
        return oldLast.data;
    }


    // Memory usage
    // class overhead: 16
    // parent pointer 8
    // pointers: 3x8
    // padding: 0
    // Total: 48
    private class Element {
        private Element nextItem;
        private Element prevItem;
        private Item data;
    }
    
    @Override
    public Iterator<Item> iterator() {
         return new DequeIterator();
    }
    
    private class DequeIterator implements Iterator<Item> {
        private Element next;
        
        public DequeIterator() {
            this.next = firstItem;
        }
        
        @Override
        public boolean hasNext() {
            return (this.next != null);
        }

        @Override
        public Item next() {
            if (this.next == null) {
                throw (new NoSuchElementException("The iteration has no more elements"));
            }
            Element nextItem = this.next;
            this.next = this.next.nextItem;
            return nextItem.data;
        }
        
        @Override
       public void remove() {
            throw new UnsupportedOperationException("remove not implewmented");
        }
    }

    public static void main(String[] args) {
         Tester t = new Tester();
        
         // test empty
         emptyTests(t);
         initialAddFirstTests(t);
         removeFirstOnlyElementTests(t);
         initialAddLastTests(t);
         removeLastOnlyElementTests(t);
     
         addMultipleTests(t);
         removeFirstTests(t);
         removeLastTests(t);
         
         t.report();
    }
    private static void removeLastTests(Tester t) {
        StdOut.println();
        StdOut.println("Removing element from End");
        StdOut.println();
        
        Deque<Integer> dq = new Deque<>();
        dq.addFirst(3);
        dq.addFirst(2);
        dq.addFirst(1);
        
        t.assertTrue("Checking remove last", (dq.removeLast() == 3));
       
        Iterator<Integer> it = dq.iterator();
        t.assertTrue("Checking remove last size", (dq.size() == 2));
        t.assertTrue("Checking remove last 1", (it.next() == 1));
        t.assertTrue("Checking remove last 2", (it.next() == 2));
        t.assertFalse("Checking iterator remove last", it.hasNext());
    }

    private static void removeFirstTests(Tester t) {
        StdOut.println();
        StdOut.println("Removing element from Front");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        dq.addFirst(3);
        dq.addFirst(2);
        dq.addFirst(1);

        t.assertTrue("Checking remove first", (dq.removeFirst() == 1));
       
        Iterator<Integer> it = dq.iterator();
        t.assertTrue("Checking remove first size", (dq.size() == 2));
        t.assertTrue("Checking remove first 2", (it.next() == 2));
        t.assertTrue("Checking remove first 3", (it.next() == 3));
        t.assertFalse("Checking iterator remove first", it.hasNext());
    }

    private static void addMultipleTests(Tester t) {
        StdOut.println();
        StdOut.println("Testing adding multiple Entries");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        dq.addFirst(3);
        dq.addFirst(2);
        dq.addLast(4);
        dq.addFirst(1);
        dq.addLast(5);        
        t.assertIllegalArgumentException("Checking null addFirst", () -> dq.addFirst(null));
        t.assertIllegalArgumentException("Checking null addLast", () -> dq.addLast(null));
        t.assertTrue("Checking add multiple last size", (dq.size() == 5));
        Iterator<Integer> it = dq.iterator();
        t.assertTrue("Checking add multiple 1", (it.next() == 1));
        t.assertTrue("Checking add multiple 2", (it.next() == 2));
        t.assertTrue("Checking add multiple 3", (it.next() == 3));
        t.assertTrue("Checking add multiple 4", (it.next() == 4));
        t.assertTrue("Checking add multiple 5", (it.next() == 5));
        t.assertFalse("Checking iterator add multiple hasNext", it.hasNext());
    }

    private static void removeLastOnlyElementTests(Tester t) {
        StdOut.println();
        StdOut.println("Removing only element from End");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        dq.addFirst(1);
        t.assertTrue("Checking remove last value", (dq.removeLast() == 1));
        t.assertTrue("Checking remove last size", (dq.size() == 0));
        t.assertTrue("Checking remove last isEmpty",dq.isEmpty());
        t.assertNoSuchElement("Checking remove last removeFirst", dq::removeFirst);
        t.assertNoSuchElement("Checking remove last removeLast", dq::removeLast);
        Iterator<Integer> it = dq.iterator();
        t.assertFalse("Checking remove last iterator hasNext", it.hasNext());
        t.assertNoSuchElement("Checking remove last iterator next", it::next);
   }

    private static void initialAddLastTests(Tester t) {
        StdOut.println();
        StdOut.println("Testing add to end of empty Deque");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        dq.addLast(1);
        t.assertTrue("Checking size", (dq.size() == 1));
        t.assertFalse("Checking isEmpty",dq.isEmpty());
        Iterator<Integer> it = dq.iterator();
        t.assertTrue("Checking empty iterator hasNext", it.hasNext());
        t.assertTrue("Checking Get First", (it.next() == 1));
        t.assertFalse("Checking iterator hasNext", it.hasNext());
        t.assertNoSuchElement("Checking empty iterator next", it::next);
    }

    private static void removeFirstOnlyElementTests(Tester t) {
        StdOut.println();
        StdOut.println("Removing only element from front");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        dq.addFirst(1);
        t.assertTrue("Checking remove first value", (dq.removeFirst() == 1));
        t.assertTrue("Checking remove first size", (dq.size() == 0));
        t.assertTrue("Checking remove first isEmpty",dq.isEmpty());
        t.assertNoSuchElement("Checking remove first removeFirst", dq::removeFirst);
        t.assertNoSuchElement("Checking remove first removeLast", dq::removeLast);
        Iterator<Integer> it = dq.iterator();
        t.assertFalse("Checking remove first iterator hasNext", it.hasNext());
        t.assertNoSuchElement("Checking remove first iterator next", it::next);
    }

    private static void initialAddFirstTests(Tester t) {
        StdOut.println();
        StdOut.println("Testing add to front of empty Deque");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        dq.addFirst(1);
        t.assertTrue("Checking size", (dq.size() == 1));
        t.assertFalse("Checking isEmpty",dq.isEmpty());
        Iterator<Integer> it = dq.iterator();
        t.assertTrue("Checking empty iterator hasNext", it.hasNext());
        t.assertTrue("Checking Get First", (it.next() == 1));
        t.assertFalse("Checking iterator hasNext", it.hasNext());
        t.assertNoSuchElement("Checking empty iterator next", it::next);
    }

    private static void emptyTests(Tester t) {
        StdOut.println();
        StdOut.println("Testing empty Deque");
        StdOut.println();

        Deque<Integer> dq = new Deque<>();
        t.assertTrue("Checking empty size", (dq.size() == 0));
        t.assertTrue("Checking empty isEmpty",dq.isEmpty());
        t.assertNoSuchElement("Checking empty removeFirst", dq::removeFirst);
        t.assertNoSuchElement("Checking empty removeLast", dq::removeLast);
        Iterator<Integer> it = dq.iterator();
        t.assertFalse("Checking empty iterator hasNext", it.hasNext());
        t.assertNoSuchElement("Checking empty iterator next", it::next);
    }

    private interface MethodUnderTest {
        void test();
    }

    private static class Tester {
        private int numFails = 0;
        
        public void assertNoSuchElement(String msg, MethodUnderTest m) {
            boolean thrown = false;
            try {
               m.test();
            } catch (NoSuchElementException e) {
                    thrown = true;
            }
            StdOut.printf("%s - %s\n", msg, isOk(thrown));
        }
        
        public void assertIllegalArgumentException(String msg, MethodUnderTest m) {
            boolean thrown = false;
            try {
               m.test();
            } catch (IllegalArgumentException e) {
                    thrown = true;
            }
            StdOut.printf("%s - %s\n", msg, isOk(thrown));
        }

        public void report() {
            StdOut.println();
            if (numFails == 0) {
                StdOut.println("All Tests Passed.");
                return;
            }
            StdOut.printf("%d Tests Failed\n", numFails);
        }

        public void assertTrue(String msg, boolean condition) {
            StdOut.printf("%s - %s\n", msg, isOk(condition));
        }
        public void assertFalse(String msg, boolean condition) {
            StdOut.printf("%s - %s\n", msg, isOk(!condition));
        }
        
        private String isOk(boolean b) {
            if (b) {
                return "Ok";
            }
            numFails++;
            return "Fail";
        }
    }
}
