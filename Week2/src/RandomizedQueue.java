import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

//Memory usage
//class overhead: 16
//pointers: Nx8
//count:   4
//padding: 4
//Total: 32 + Nx8
public class RandomizedQueue<Item> implements Iterable<Item> {
    
    private int entryCount;
    private Item[] items;

    // construct an empty randomized queue
    public RandomizedQueue() {
        items = (Item[])(new Object[0]);
    }
    
    // is the randomized queue empty?
    public boolean isEmpty() {
        return (entryCount == 0);
    }
    
    // return the number of items on the randomized queue
    public int size() {
        return entryCount;
    }
    
    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot enqueue a null item.");
        }
        // TODO performance unneceesary copying of RHS entries 
        // possibly resize array, shift entries right and add entry 
        if (entryCount == items.length) {
            int newSize = 2 * entryCount;
            if (entryCount == 0) {
                newSize = 2;
            }
            items = copyItems(newSize);
        }
        int index = entryCount;
        for (int i = entryCount; i > index; i--) {
            items[i] = items[i-1];
        }
        items[index] = item;
        entryCount++;
    }
    
    // remove and return a random item
    public Item dequeue() {
        if (entryCount == 0) {
            throw new NoSuchElementException("Nothing to deque");
        }
        int index = StdRandom.uniform(entryCount);
        Item item = items[index];
        //shift entries left
        // TODO performance unneceesary copying of RHS entries 
        int limit = entryCount - 1;
        for (int i = index; i < limit; i++) {
            items[i] = items[i+1];
        }
        if (entryCount != items.length) {
            items[entryCount] = null;
        }
        entryCount = limit;
        // possibly resize array
        // TODO  edge case entryCount = 0
        if (entryCount < items.length / 4) {
            items = copyItems(items.length / 2);
        }
        return item;
    }
    
    private Item[] copyItems(int size) {
        Item[] newItems = (Item[])(new Object[size]);
        for (int i = 0; i < entryCount; i++) {
            newItems[i] = items[i];
        }
        return newItems;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        if (entryCount == 0) {
            throw new NoSuchElementException("Nothing to sample");
        }
        int index = StdRandom.uniform(entryCount);
        return items[index];
    }
    
    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
         return new RandomIterator();
    }
    
    private class RandomIterator implements Iterator<Item> {
       private final Item[] iterItems;
       private int nextItem;
        
        public RandomIterator() {
            iterItems = copyItems(entryCount);
            StdRandom.shuffle(iterItems);
        }
        
        @Override
        public boolean hasNext() {
            return (nextItem < iterItems.length);
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw (new NoSuchElementException("The iteration has no more elements"));
            }
            return iterItems[nextItem++];
        }
        
        @Override
       public void remove() {
            throw new UnsupportedOperationException("remove not implewmented");
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        RandomizedQueue<String> rq = new RandomizedQueue<>();
        rq.enqueue("A");
        rq.enqueue("B");
        rq.enqueue("C");
        rq.enqueue("D");
        rq.enqueue("E");
        rq.enqueue("F");
        rq.enqueue("G");
        rq.enqueue("H");
        rq.enqueue("I");
        Iterator<String> it1 = rq.iterator();
        Iterator<String> it2 = rq.iterator();
        rq.printIterator(it1);
        rq.printIterator(it2);
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("queue size %d\n",rq.size());     
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
        StdOut.printf("dequed %s\n",rq.dequeue());     
        rq.printArray(rq.items, rq.entryCount);
    }

    private void printIterator(Iterator<?> it) {
        while(it.hasNext()) {
            StdOut.print(it.next().toString());
        }
        StdOut.println();
    }

    private void printArray(Object[] arr, int numItems) {
        for (int i = 0; i < numItems; i++) {
            StdOut.print(arr[i].toString());
        }
        StdOut.println();
    }
    
}
