import java.util.HashMap;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.FlowEdge;
import edu.princeton.cs.algs4.FlowNetwork;
import edu.princeton.cs.algs4.FordFulkerson;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

// create a baseball division from given filename in format specified below
public class BaseballElimination {
    private final int numTeams;
    private final HashMap<String, Integer> teams;
    private final String[] names;
    private final int[] wins;
    private final int[] loss;
    private final int[] remain;
    private final int[][] games;
    private final int[][] eliminatedBy;
    private final int numVertices;
    private final int targetId;
    private final int sourceId;

    public BaseballElimination(String filename) {
        In in = new In(filename);
        numTeams = in.readInt();
        teams = new HashMap<>();
        names = new String[numTeams];
        wins = new int[numTeams];
        loss = new int[numTeams];
        remain = new int[numTeams];
        games = new int[numTeams][numTeams];
        eliminatedBy = new int[numTeams][];

        for (int i = 0; i < numTeams; i++) {
            names[i] = in.readString();
            teams.put(names[i], i);
            wins[i] = in.readInt();
            loss[i] = in.readInt();
            remain[i] = in.readInt();
            int[] against = new int[numTeams];
            games[i] = against;
            for (int j = 0; j < numTeams; j++) {
                against[j] = in.readInt();
            }
        }

        // 1 source, 1 target, n-1 teams, S(n-2) other edges = 2 + S(n-1) vertices
        numVertices = 2 + (numTeams * (numTeams - 1)) / 2;
        targetId = numTeams - 1;
        sourceId = numTeams;

        for (int team = 0; team < numTeams; team++) {
            if (trivialElimination(team)) {
                continue;
            }
            int teamMaxPossible = wins[team] + remain[team];
            FlowNetwork f = new FlowNetwork(numVertices);
            
            int[] vertex2Team = new int[numTeams - 1];
            int[] team2Vertex = new int[numTeams];
            team2Vertex[team] = -1;
            int v = 0;
            for (int i = 0; i < numTeams; i++) {
                if (i == team) { continue; }
                vertex2Team[v] = i;
                team2Vertex[i] = v;
                f.addEdge(new FlowEdge(v++, targetId, (teamMaxPossible - wins[i])));
            }
            v = v + 2;  // reserve for source and target vertecies
            for (int i : vertex2Team) {
                if (i == team) { continue; }
                for (int j = i + 1; j < numTeams; j++) {
                    if (j == team) { continue; }
                    f.addEdge(new FlowEdge(sourceId, v, games[i][j]));
                       f.addEdge(new FlowEdge(v, team2Vertex[i], Double.POSITIVE_INFINITY));
                       f.addEdge(new FlowEdge(v++, team2Vertex[j], Double.POSITIVE_INFINITY));
                }
            }
            
            FordFulkerson ff = new FordFulkerson(f, sourceId, targetId);
            if (!allEdgesFull(f,sourceId )) {
                eliminatedBy[team] = inCut(ff, vertex2Team);
            }
        }
    }
        
    private int[] inCut(FordFulkerson ff, int[] vertex2Team) {
        Bag<Integer> bag = new Bag<>();
        for (int j = 0; j < numTeams; j++) {
            if (ff.inCut(j)) {
                bag.add(j);
            }
        }
        int[] ret = new int[bag.size()];
        int i = 0;
        for (int t : bag) {
            ret[i++] = vertex2Team[t];
        }
        return ret;
    }


    private boolean allEdgesFull(FlowNetwork f, int id) {
           for (FlowEdge e : f.adj(sourceId)) {
               if (e.capacity() != e.flow()) {
                   return false;
               }
           }
        return true;
    }
    
    // number of teams
    public int numberOfTeams() {
        return numTeams;    
    }

    // all teams
    public Iterable<String> teams() {
        return this.teams.keySet();
    }
    // number of wins for given team
    public int wins(String team) {
        int t = checkValidTeam(team);
        return wins[t];
    }
    
    // number of losses for given team
    public int losses(String team) {
        int t = checkValidTeam(team);
        return loss[t];
    }
    
    // number of remaining games for given team
    public int remaining(String team) {
        int t = checkValidTeam(team);
        return remain[t];
    }

    // number of remaining games between team1 and team2
    public int against(String team1, String team2) {
        int t1 = checkValidTeam(team1);
        int t2 = checkValidTeam(team2);
        return games[t1][t2];    
    }

    // is given team eliminated?
    public boolean isEliminated(String team) {
        int t = checkValidTeam(team);
        return (eliminatedBy[t].length > 0);
    }

    private boolean trivialElimination(int t) {
        int best = wins[t] + remain[t];
        for (int j = 0; j < numTeams; j++) {
            if (wins[j] > best) {
                eliminatedBy[t] = new int[1];
                eliminatedBy[t][0] = j;
                return true;
            }
        }
        eliminatedBy[t] = new int[0];
        return false;
    }

    // subset R of teams that eliminates given team; null if not eliminated
    public Iterable<String> certificateOfElimination(String team) {
        int t = checkValidTeam(team);
        if (!isEliminated(team)) {
            return null;
        }
        
        Bag<String> certs = new Bag<>();
        for (int j : eliminatedBy[t]) {
            certs.add(names[j]);
        }
        return certs;
    }

    private int checkValidTeam(String team) {
        Integer t = teams.get(team);
        if (t == null) {
            throw new IllegalArgumentException(team + " is an invalid team.");
        }
        return t;
    }

    public static void main(String[] args) {
        BaseballElimination division = new BaseballElimination(args[0]);
        for (String team : division.teams()) {
            if (division.isEliminated(team)) {
                StdOut.print(team + " is eliminated by the subset R = { ");
                for (String t : division.certificateOfElimination(team)) {
                    StdOut.print(t + " ");
                }
                StdOut.println("}");
            }
            else {
                StdOut.println(team + " is not eliminated");
            }
        }
    }
}
