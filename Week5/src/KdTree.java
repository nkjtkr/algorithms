import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdDraw;

public class KdTree {

    private static final int EQUAL = 0;
    private static final int RIGHT = 1;
    private static final int LEFT = -1;

    private Node rootNode;
    private int numPoints = 0;
    private double minDistance;
    private Point2D nearest;
    
    private enum Side {
        LHS,
        RHS,
        UPPER,
        LOWER
    }

    // construct an empty set of points
    public KdTree()  {
     }

    // is the set empty?
    public boolean isEmpty() {
        return (numPoints == 0);
    }

    // number of points in the set
    public  int size() {
        return numPoints;
    }
    
    private Node find(Point2D p) {
        Node currentNode = rootNode;
        while (currentNode != null) {
            int cmp = currentNode.compareTo(p);
            if (cmp < 0) { 
                currentNode = currentNode.leftNode;
            } else if (cmp > 0) {
                currentNode = currentNode.rightNode;
            } else { // (cmp == 0)
                return currentNode;
            }
        }
        return null;
    }

    // add the point to the set (if it is not already in the set) 
    public void insert(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        rootNode = insert(rootNode, p, false);
    }

    private void insert(double x, double y) {
        insert(new Point2D(x, y));
    }

    private Node insert(Node parent, Point2D p, boolean isHorizontal) {
        if (parent == null) {
            numPoints++;
            return new Node(p, isHorizontal);
        }
        int cmp = parent.compareTo(p);
        if (cmp < 0) {
            parent.leftNode = insert(parent.leftNode, p, !parent.isHorizontal);
        } else if (cmp > 0) {
            parent.rightNode = insert(parent.rightNode, p, !parent.isHorizontal);
        }
        return parent;
    }

    // does the set contain point p?
    public boolean contains(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        return (find(p) != null);
    }

    // draw all points to standard draw
    public void draw() {
        draw(rootNode, 0.0, 1.0, 0.0, 1.0);
    }

    private void draw(Node node, double minX, double maxX, double minY, double maxY) {
        if (node == null) {
            return;
        }
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        StdDraw.point(node.value.x(), node.value.y());
        
        StdDraw.setPenRadius(0.005);
        if (node.isHorizontal) {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(minX, node.value.y(), maxX, node.value.y());    
            if (node.leftNode != null) {
                draw(node.leftNode, minX, maxX, minY, node.value.y());
            }
            if (node.rightNode != null) {
                draw(node.rightNode, minX, maxX, node.value.y(), maxY);
            }
        } else {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(node.value.x(), minY, node.value.x(), maxY);    
            if (node.leftNode != null) {
                draw(node.leftNode, minX, node.value.x(), minY, maxY);
            }
            if (node.rightNode != null) {
                draw(node.rightNode, node.value.x(), maxX, minY, maxY);
            }
        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new IllegalArgumentException();
        }
        
        Stack<Point2D> inside = new Stack<>();
        range(rootNode, rect, inside);
        return inside;
    }
    
    private void range(Node node, RectHV rect, Stack<Point2D> inside) {
        if (node == null) {
            return;
        }
        
        Point2D p = node.value;
        if (rect.contains(p)) {
            inside.push(p);
        }
       if (node.isHorizontal) {
            if (rectBelowPoint(rect, p)) {
                range(node.leftNode, rect, inside);
            }
            if (rectAbovePoint(rect, p)) {
                range(node.rightNode, rect, inside);
            }
        } else {
            if (rectLeftOfPoint(rect, p)) {
                range(node.leftNode, rect, inside);
            }
            if (rectRightOfPoint(rect, p)) {
                range(node.rightNode, rect, inside);
            }
        }
    }

    private boolean rectBelowPoint(RectHV rect, Point2D p) {
         return (rect.ymin() <= p.y());
    }
    private boolean rectAbovePoint(RectHV rect, Point2D p) {
        return (rect.ymax() >= p.y());
   }
    private boolean rectLeftOfPoint(RectHV rect, Point2D p) {
       return (rect.xmin() <= p.x());
    }
    private boolean rectRightOfPoint(RectHV rect, Point2D p) {
        return (rect.xmax() >= p.x());
     }

    // a nearest neighbour in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new IllegalArgumentException();
        }
        if (this.isEmpty()) {
            return null;
        }
        this.minDistance = Double.POSITIVE_INFINITY;
        this.nearest = null;
        RectHV searchArea = new RectHV(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        nearest(p, rootNode, searchArea);
        return this.nearest;
     }
     
    private void nearest(Point2D p, Node node, RectHV searchArea) {
        double currDistance = p.distanceSquaredTo(node.value);
        if (currDistance < this.minDistance) {
            this.minDistance = currDistance;
            this.nearest = node.value;
        }
        // walking tree - filter out nodes
        
        RectHV leftSearchArea = null;
        RectHV rightSearchArea = null;
        if (node.isHorizontal) {
            leftSearchArea = newSearchArea(node, searchArea, Side.LOWER);
            rightSearchArea = newSearchArea(node, searchArea, Side.UPPER);
        } else {
            leftSearchArea = newSearchArea(node, searchArea, Side.LHS);
            rightSearchArea = newSearchArea(node, searchArea, Side.RHS);
        }
        double leftDistance = Double.POSITIVE_INFINITY;
        double rightDistance = Double.POSITIVE_INFINITY;
        
        if (node.leftNode != null) {
            leftDistance = leftSearchArea.distanceSquaredTo(p);
        }

        if (node.rightNode != null) {
            rightDistance = rightSearchArea.distanceSquaredTo(p);
        }
        
        boolean leftFirst = (leftDistance <= rightDistance);
        if (leftFirst) {
            if (leftDistance < this.minDistance) {
                nearest(p, node.leftNode, leftSearchArea);
            }
        }
        if (rightDistance < this.minDistance) {
            nearest(p, node.rightNode, rightSearchArea);
        }
        if (!leftFirst) {
            if (leftDistance < this.minDistance) {
                nearest(p, node.leftNode, leftSearchArea);
            }
        }
    }

    private RectHV newSearchArea(Node node, RectHV searchArea, Side side) {
        switch(side) {
        case LHS:
            return new RectHV(searchArea.xmin(), searchArea.ymin(),
                            node.value.x(), searchArea.ymax());
        case RHS:
            return new RectHV(node.value.x(), searchArea.ymin(),
                            searchArea.xmax(), searchArea.ymax());
        case UPPER:
            return new RectHV(searchArea.xmin(), node.value.y(),
                            searchArea.xmax(), searchArea.ymax());
        default:
            return new RectHV(searchArea.xmin(), searchArea.ymin(),
                            searchArea.xmax(), node.value.y());
        }
    }

    private class Node {
        final boolean isHorizontal;
        Node leftNode = null;
        Node rightNode = null;
        final Point2D value;
        
        public Node(Point2D value, boolean isHorizontal) {
            this.value = value;
            this.isHorizontal = isHorizontal;
        }

        public int compareTo(Point2D p) {
            if (isHorizontal) {
                if (p.y() < this.value.y()) {
                    return LEFT;
                } else if (p.y() > this.value.y()) {
                    return RIGHT; 
                } else if (p.x() != this.value.x()) {
                    return RIGHT;
                 } else {
                    return EQUAL;
                }
            } else {
                if (p.x() < this.value.x()) {
                    return LEFT;
                } else if (p.x() > this.value.x()) {
                    return RIGHT;
                } else if (p.y() != this.value.y()) {
                    return RIGHT;
               } else {
                    return EQUAL;
                }
            }
        }
    }

    // unit testing of the methods (optional)
    public static void main(String[] args) {
        KdTree points = new KdTree();
        points.insert(0.7, 0.2);
        points.insert(0.5, 0.4);
        points.insert(0.2, 0.3);
        points.insert(0.4, 0.7);
        points.insert(0.9, 0.6);

        PointSET brute = new PointSET();
        brute.insert(new Point2D(0.7, 0.2));
        brute.insert(new Point2D(0.5, 0.4));
        brute.insert(new Point2D(0.2, 0.3));
        brute.insert(new Point2D(0.4, 0.7));
        brute.insert(new Point2D(0.9, 0.6));
        
        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 1);
        StdDraw.setYscale(0, 1);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        StdDraw.show();

        // process nearest neighbor queries
        StdDraw.enableDoubleBuffering();
        while (true) {

            // the location (x, y) of the mouse
            double x = StdDraw.mouseX();
            double y = StdDraw.mouseY();
            
            Point2D query = new Point2D(x, y);

            // draw all of the points
            StdDraw.clear();
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(0.01);
            points.draw();

            StdDraw.setPenRadius(0.04);
            StdDraw.setPenColor(StdDraw.GREEN);
            query.draw();
            
            StdDraw.setPenRadius(0.03);
            StdDraw.setPenColor(StdDraw.RED);
            Point2D bNear = brute.nearest(query);
            bNear.draw();

            StdDraw.setPenRadius(0.02);
            StdDraw.setPenColor(StdDraw.BLUE);
            Point2D kdNear = points.nearest(query);
            kdNear.draw();
            
            StdDraw.show();
            StdDraw.pause(40);
        }
    } 

}
