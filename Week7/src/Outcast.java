import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

public class Outcast {

    private final WordNet wordnet;

    // constructor takes a WordNet object
    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns) {
        if (nouns.length == 0) {
            throw new IllegalArgumentException("empty list supplied");
        }
        if (nouns.length == 1) {
            return nouns[0];
        }
        if (nouns.length == 2) {
            return nouns[1];
        }

        int distMax = 0;
        int indexMax = 0;
        
        for (int i = 0; i < nouns.length; i++) {
            int dist = 0;
            for (int j = 0; j < nouns.length; j++) {
                if (i == j) { continue; }
                dist += wordnet.distance(nouns[i], nouns[j]);
            }
            if (dist > distMax) {
                distMax = dist;
                indexMax = i;
            }
        }
        return nouns[indexMax];
    }

    public static void main(String[] args) {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
    }

}