import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.ST;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Topological;

public class WordNet {
    private final String[] synset;
    private final ST<String, Bag<Integer>> words = new ST<>();
    private final SAP sap;

    // constructor takes the name of the two input files
    // this needs to run in linear time
    public WordNet(String synsets, String hypernyms) {
        if (synsets == null || hypernyms == null) {
            throw new IllegalArgumentException();
        }
        try {
            this.synset = loadSynsets(synsets);
            Digraph digraph = loadDigraph(hypernyms, synset.length);

            // is the input a rooted DAG
            Topological top = new Topological(digraph);
            if (!top.hasOrder()) {
                throw new IllegalArgumentException("Not a DAG"); 
            }
            // the DAG is rooted if exactly one vertex has no outward edges
            int count = 0;
            for (Integer item : top.order()) {
                if (digraph.outdegree(item) == 0) {
                    count++;
                }
            }
            if (count != 1) {
                throw new IllegalArgumentException("Not a rooted DAG"); 
           }
            this.sap = new SAP(digraph);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("problem converting a string to an int", nfe);
        }
    }

     /**
     * @param filename
     * @return 
     * @throws IllegalArgumentException if the filename is null or cannot be opened
     * @throws NumberFormatException if an integer conversion fails
     */
    private Digraph loadDigraph(String filename, int numVertices) {
        Digraph dg = new Digraph(numVertices);

        In in = new In(filename);
        while (in.hasNextLine()) {
            String line = in.readLine();
            String[] fld = line.split(",");
            int id = Integer.parseInt(fld[0]);
            for (int i = 1; i < fld.length; i++) {
                dg.addEdge(id, Integer.parseInt(fld[i]));
            }
        }
        in.close();
        return dg;
    }
    /**
     * @param filename
     * @return 
     * @throws IllegalArgumentException if the filename is null or cannot be opened
     * @throws NumberFormatException if an integer conversion fails
     */
    private String[] loadSynsets(String filename) {
        Stack<String> stk = loadFile(filename);
        if (stk.isEmpty()) {
            throw new IllegalArgumentException();
        }
        int maxId = lastId(stk);
        String[] arr = new String[maxId + 1];

        for (String line : stk) {
            String[] fld = line.split(",");
            int id = Integer.parseInt(fld[0]);
            String[] wordlist = fld[1].split(" ");
            arr[id] = fld[1];
            for (String word : wordlist) {
                Bag<Integer> bag = words.get(word);
                if (bag == null) {
                    bag = new Bag<>();
                    words.put(word, bag);
                }
                bag.add(id);
            }
        }
        return arr;
    }

    // get the id held on the last record
    private int lastId(Stack<String> stk) {
        String lastRecord = stk.peek();
        String[] fld = lastRecord.split(",");
        return Integer.parseInt(fld[0]);
    }

    private Stack<String> loadFile(String filename) {
        Stack<String> stk = new Stack<>();
        In in = new In(filename);
        while (in.hasNextLine()) {
            stk.push(in.readLine());
        }
        in.close();
        return stk;
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return words.keys();
    }

    // is the word a WordNet noun?
    // this needs to run in logarithmic time...
    public boolean isNoun(String word) {
        return words.contains(word);
    }

    // distance between nounA and nounB
    public int distance(String nounA, String nounB) {
        Bag<Integer> bagA = words.get(nounA);
        Bag<Integer> bagB = words.get(nounB);
        if ((bagA == null) || (bagB == null)) {
            throw new IllegalArgumentException("word not found");
        }
        return this.sap.length(bagA, bagB);
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path
    public String sap(String nounA, String nounB) {
        Bag<Integer> bagA = words.get(nounA);
        Bag<Integer> bagB = words.get(nounB);
        if ((bagA == null) || (bagB == null)) {
            throw new IllegalArgumentException("word not found");
        }
        int id = sap.ancestor(bagA, bagB);
        if (id < 0) {
            return null;
        }
        return synset[id];
    }

    // do unit testing of this class
    public static void main(String[] args) {
        // test loading the structures
        String word1;
        String word2;
        WordNet w;
        if (args.length < 4) {
            w = new WordNet("file:./Week7/synsets.txt", "file:./Week7/hypernyms.txt");
            word1 = "Ambrose";
            word2 = "St._Ambrose";           
        } else {
            w = new WordNet(args[0], args[1]);
            word1 = args[2];
            word2 = args[3];           
            
        }
        String anc = w.sap(word1, word2);
        StdOut.println(anc);
        int distance = w.distance(word1, word2);
        StdOut.println(distance);
    }
}
