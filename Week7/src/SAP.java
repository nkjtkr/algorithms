import java.util.HashMap;
import java.util.Objects;

import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class SAP {

    private static final int NOT_FOUND = -1;
    private final HashMap<KeyPair, Ancestor> cache = new HashMap<>();

    private final Digraph digraph;
    private final int numVertices;

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph g) {
        this.digraph = new Digraph(g);
        this.numVertices= this.digraph.V();
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        checkRange(v);
        checkRange(w);

        Ancestor a = getAncestor(v, w);
        return a.distance;
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        checkRange(v);
        checkRange(w);

        Ancestor a = getAncestor(v, w);
        return a.id;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        Ancestor a = commonAncestor(v, w);
        return a.distance;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        Ancestor a = commonAncestor(v, w);
        return a.id;
    }

    private void checkRange(int v) {
        if (v < 0 || v >= this.numVertices) {
            throw new IllegalArgumentException("invalid value");
        }        
    }

    private void checkRange(Iterable<Integer> iter) {
        if (iter == null) {
            throw new IllegalArgumentException("null iterable");
        }
        for (Integer i : iter) { 
            if (i == null) {
                throw new IllegalArgumentException("null value");
            }
            checkRange(i);
        }
    }

    private Ancestor commonAncestor(Iterable<Integer> v, Iterable<Integer> w) {
        checkRange(v);
        checkRange(w);

        int shortest = Integer.MAX_VALUE;
        int commonAncestor = NOT_FOUND;
        for (int i : v) {
            for (int j : w) {
                Ancestor a = getAncestor(i, j);
                if (a.id != NOT_FOUND) {
                    int len = a.distance;
                    if (len < shortest) {
                        shortest = len;
                        commonAncestor = a.id;
                    }
                }
            }
        }

        if (commonAncestor == NOT_FOUND) {
            shortest = NOT_FOUND;
        }
        return new Ancestor(commonAncestor, shortest);
    }

    private class KeyPair {
        private final int a;
        private final int b;
        public KeyPair(int a, int b) {
            if (a < b) {
                this.a = a;
                this.b = b;
            } else {
                this.a = b;
                this.b = a;
            }
        }
        public final int getA() {
            return a;
        }
        public final int getB() {
            return b;
        }
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
             result = prime * result + Objects.hash(a, b);
            return result;
        }
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            KeyPair other = (KeyPair) obj;
            return a == other.a && b == other.b;
        }
    }

    private Ancestor getAncestor(int v, int w) {
        if (v == w) {
            return new Ancestor(v, 0);
        }
        KeyPair key = new KeyPair(v, w);
        Ancestor value = cache.get(key);
        if (value != null) {
            return value;
        }
        Ancestor common = new Ancestor(NOT_FOUND, Integer.MAX_VALUE);
        
         BFS vSearch = new BFS(digraph, v);
         BFS wSearch = new BFS(digraph, w);
         vSearch.setLinkedSearch(wSearch);
         wSearch.setLinkedSearch(vSearch);

         // lock step BFS from both v and w
        while (!(vSearch.isFinished() && wSearch.isFinished())) {
            if (!vSearch.isFinished()) {
                vSearch.step(common);
            }
            if (!wSearch.isFinished()) {
                wSearch.step(common);
            }
        }

/*
0 -> 1 -> 2 -> 6 length 3
6 ->             length 0
                 total length 3
                 
0 -> 1 -> 2 -> 6 length 3
5- > 6           length 1
0 -> 1 -> 2 -> 6 <- 5 length 4

*/
        if (common.distance == Integer.MAX_VALUE) {
            common.distance = NOT_FOUND;
        }
        
        // break cyclic dependency
        vSearch.setLinkedSearch(null);
        wSearch.setLinkedSearch(null);

        cache.put(key,common);
        return common;
    }

    private class BFS {
        
        private int[] distance;
        private int[] parents;
        private boolean[] visited;
        private Queue<int[]> q;
        private Digraph digraph;
        private BFS other;
        
        BFS(Digraph digraph, int start) {
            this.digraph = digraph;
            int numVert = digraph.V();
            distance = new int[numVert];
            parents = new int[numVert];
            visited = new boolean[numVert];
            q = new Queue<>();
            int[] elt = {start, NOT_FOUND}; 
            q.enqueue(elt);
        }
        
        public boolean isFinished() {
            return q.isEmpty();
        }

        void step(Ancestor common) {
            int[] elt = q.dequeue();
            int a = elt[0];
            int parent = elt[1];
            if (visited[a]) {
                return;
            }

            visited[a] = true;
            parents[a] = parent;
            int dist = 0;
            if (parent != NOT_FOUND) {
                dist = distance[parent] + 1;
            }
            distance[a] = dist;
            
            if (dist >= common.distance) {
                return;
            }

            if (foundCommonAncestor(a)) {
                updateCommonAncestor(common, a);
                return;
            }
            
            for (int b : digraph.adj(a)) {
                if (!visited[b]) {
                    int[] e = {b, a};
                    q.enqueue(e);
                }
            }
        }

        private boolean foundCommonAncestor(int a) {
            return this.visited[a] && other.visited[a];
        }

        private void updateCommonAncestor(Ancestor common, int a) {
            int dist = distance[a] + other.distance[a];
            if (dist < common.distance) {
                common.distance = dist;
                common.id = a;
            }
        }

       public void setLinkedSearch(BFS linkedSearch) {
            this.other = linkedSearch;
        }
    }

    private class Ancestor {
        int distance;
        int id;
        public Ancestor(int ancestor, int distance) {
            this.id = ancestor;
            this.distance = distance;
        }
    }

    // do unit testing of this class
    public static void main(String[] args) {
        In in = new In("digraph-wordnet.txt");
        SAP sap = new SAP(new Digraph(in));
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length   = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}
